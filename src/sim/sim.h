// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
/**
 @file
 @brief Global Compile Switches
*/

#ifndef SIM_H
#define SIM_H

/**
 Enable code to be able to read old trajectory files
 Option normally ON
 */
#define BACKWARD_COMPATIBILITY


/**
 Enables Myosin, Kinesin and Dynein
 Option normally OFF
 */
#define NEW_HANDS 0


#endif
